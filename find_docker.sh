#!/bin/bash

ADDRESSES=$(ip a | grep inet | awk '{ print $2 }' | awk -F'/' '{ print $1 }' | grep -v -e "127.0.0.1";)

FOUND_DOCKER=false
while read -r ADDRESS; do
    echo "Found address $ADDRESS" >&2;
    LEFT=$(awk -F'.' '{ print $1 "." $2 "." $3 "." }' <<< $ADDRESS);
    echo "Left: $LEFT" >&2;
    for i in {1..254}; do
	P_LEFT=tcp://$LEFT
	P_RIGHT=:2375/
        export DOCKER_HOST=$P_LEFT$i$P_RIGHT
        docker ps >/dev/null 2>&1 && FOUND_DOCKER=true && break;
    done;
    if [[ "$FOUND_DOCKER" -eq "true" ]]; then
        echo "Found docker at $DOCKER_HOST" >&2;
        echo $DOCKER_HOST;
	exit;
    fi;
done <<< $ADDRESSES;
exit 1;

