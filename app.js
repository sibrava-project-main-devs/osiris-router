require('dotenv').config();

let url = require('url');
let http = require('http');

let listeningPort = 80;
let apiHost = process.env.HOST_API === undefined ? "localhost" : process.env.HOST_API;
let apiPort = process.env.PORT_API === undefined ? 8500 : parseInt(process.env.PORT_API);
let webappHost = process.env.HOST_APP === undefined ? "localhost" : process.env.HOST_APP;
let webappPort = process.env.PORT_APP === undefined ? 8510 : parseInt(process.env.PORT_APP);

let server = http.createServer(function(request, response) {
    if(request.method == "OPTIONS"){
        // we need to send the raw back
        console.log("Clearing pre-flight check (CORS)");
        response.setHeader("Access-Control-Allow-Origin", process.env.ORIGIN_URL);
        response.setHeader("Access-Control-Allow-Credentials", true);
        response.setHeader("Access-Control-Allow-Methods", "get, post, options, put, delete, head");
        response.setHeader("Access-Control-Allow-Headers", ["content-type"]);
        response.setHeader("Access-Control-Max-Age", "86400");
        response.writeHead(200);
        response.end();
        return;
    }

    let requestUrl = request.url;
    console.log("Received request for '" + requestUrl + "'");

    // requestUrl will start with /api/ for api, everything else is the web app
    let isApi = requestUrl.startsWith("/api/");
    let targetHost = isApi ? apiHost : webappHost;
    let targetPort = isApi ? apiPort : webappPort;

    if(isApi){
        requestUrl = requestUrl.substr("/api".length);
        if(requestUrl.length === 0){
            requestUrl = "/";
        }else if(requestUrl[0] !== '/'){
            requestUrl = "/" + requestUrl;
        }
    }

    request.pause();

    let options = url.parse("http://" + targetHost + ":" + targetPort + requestUrl);
    options.headers = request.headers;
    options.method = request.method;
    options.agent = request.agent;
    options.headers['host'] = targetHost;
    options.headers['X-Source-IP'] = request.socket.remoteAddress;

    console.log("Routing to " + targetHost + ":" + targetPort);

    let connector = http.request(options, function(serverResponse){
            console.log("Received " + serverResponse.statusCode + " response for '" + requestUrl + "'");
            serverResponse.pause();
            serverResponse.headers["Access-Control-Allow-Origin"] = process.env.ORIGIN_URL;
            serverResponse.headers["Access-Control-Allow-Credentials"] = true;
            serverResponse.headers["Access-Control-Allow-Headers"] = ["content-type"];
            response.writeHeader(serverResponse.statusCode, serverResponse.headers);
            serverResponse.pipe(response, {end: true});
            serverResponse.resume();
    });
    connector.on('error', function(){
        console.log("Route target (" + targetHost + ":" + targetPort + ") wasn't able to respond");
    })

    request.pipe(connector, {end: true});
    request.resume();
});

console.log("Router is listening on port " + listeningPort);
console.log("Will route API requests to " + apiHost + ":" + apiPort + ", everything else to " + webappHost + ":" + webappPort);
server.listen(listeningPort);
