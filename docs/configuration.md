### .env file
- `HOST_API` - URL address on which the API is running
- `PORT_API` - Port on which the API is running
- `HOST_APP` - URL address on which the client application is running
- `PORT_APP` - Port on which the client application is running
- `ORIGIN_URL` - Origin URL used in CORS Access-Control-Allow-Origin header