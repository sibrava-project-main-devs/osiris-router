FROM node:8
LABEL maintainer="Freem~ <freemanovec@protonmail.com>"

WORKDIR /usr/src/app

COPY . .
RUN yarn install

EXPOSE 80
ENTRYPOINT [ "nodejs", "app.js" ]
