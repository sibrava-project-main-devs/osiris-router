## Development guide
1. Clone the repo from GitLab
2. Add `.env` file based on `.env.example`
3. Start up the router with `yarn start`

For info on how to set up `.env` file, see https://sibrava-project-main-devs.gitlab.io/osiris-router/#/configuration

### Documentation
Documentation can be found on: https://sibrava-project-main-devs.gitlab.io/osiris-router